<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpeningHour extends Model
{
    protected $table = "opening_hour";
    public $timestamps = false;
}
