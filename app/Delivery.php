<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = "delivery";

    function getOrder() {
      return $this->hasOne('App\Order','id','order_id');
    }
}
