<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;
use App\Order;
use App\Product;
use App\Review;

class OrderController extends Controller
{

  public function getOrdersById($order_id){
    $order = Order::where('id','=',$order_id)->first();
    $orderedByCompany = $order->getCompany;
    $locationOfCompany = $orderedByCompany->getLocation;
    return $order;
  }


  public function storeOrder(Request $request){
    $order = new Order();
    $order->products_in_order = $request->input('products_in_order');
    $order->ordered_by_company_id = $request->input('ordered_by_company_id');
    $order->costs = $request->input('costs');
    $order->payment_method = $request->input('payment_method');
    $order->delivery_time = $request->input('delivery_time');
    $order->payment_complete = false;
    $order->description = $request->input('description');
    $order->created_at = $request->input('created_at');
    $order->updated_at = $request->input('updated_at');
    $order->deleted_at = $request->input('deleted_at');

    try{
    $order->save();

    }
    catch(Exception $e){
      return redirect('/');
    }
  }

  public function update(Request $request){
       $order = Order::where('id', '=', $request->$order_id);
       $order->products_in_order= $request->input('products_in_order');
       $order->ordered_by_company_id = $request->input('ordered_by_company_id');
       $order->costs = $request->input('costs');
       $order->payment_method = $request->input('payment_method');
       $order->delivery_time = $request->input('delivery_time');
       $order->payment_complete = false;
       $order->description = $request->input("description");
       $order->created_at = $request->input('created_at');
       $order->updated_at = $request->input('updated_at');
       $order->deleted_at = $request->input('deleted_at');

       $order->save();

     try{
     $order->save();
      return redirect('/');
      }
      catch(Exception $e){
        return redirect('/');
     }


   }

  public function getOrders(){
    return Order::all();
  }

  public function getOrdersByCompanyId($companyId) {
    $order = Order::where('ordered_by_company_id', $companyId);
    if ($order->exists()) {
      $order = $order->get();
      return response()->json($order, 200);
    } else {
      return response()->json(
        ["message" => "No products found for the company with company id: " . $companyId . "."],
        404
      );
    }
  }
}
