<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
  public function getProductsByCompany($companyId) {
    $products = Product::where('company_id', $companyId);
    if ($products->exists()) {
      $products = $products->get();
      return response()->json($products, 200);
    } else {
      return response()->json(
        ["message" => "No products found for the company with company id: " . $companyId . "."],
        404
      );
    }
  }

  public function getProductLikeName($companyId, $productName) {
    $likeProductName = "%" . $productName . "%";
    $products = Product::where("company_id", "=", $companyId)->where("name", "like", $likeProductName)->orWhere("company_id", "=", $companyId)->where("description", "like", $likeProductName);
    if ($products->exists()) {
      $products = $products->get();
      return response()->json($products, 200);
    } else {
      return response()->json(
        ["message" => "No products found for the company with company id: " . $companyId . ", with a name like: " . $productName . "."],
        404
      );
    }
  }

  public function storeProduct(Request $request){
    $product = new Product();
    $product->name = $request->input('name');
    $product->description = $request->input('description');
    $product->photo = $request->input('photo');
    $product->allergies = $request->input('allergies');
    $product->price = $request->input('price');
    $product->company_id = $request->input('company_id');
    $product->created_at = $request->input('created_at');
    $product->updated_at = $request->input('updated_at');
    $product->deleted_at = $request->input('deleted_at');

    try{
    $product->save();

    }
    catch(Exception $e){
      return redirect('/');
    }
  }
}
