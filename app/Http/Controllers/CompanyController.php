<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;
use App\Order;
use App\Product;
use App\Review;

class CompanyController extends Controller
{
    public function getCompanyName(){
      return Company::all();
    }

  public function getCompanyById($companyId) {
    $company = Company::with("locations")->with("openingHour")->find($companyId);
    if ($company) {
      return response()->json($company, 200);
    } else {
      return response()->json(["message" => "No company found with company id: " . $companyId . "."], 404);
    }
  }
  
  public function getAllCompanies(){
    return Company::all();
  }


  public function getCompanyLikeName($companyName) {
    $company = Company::where("name", "like", "%" . $companyName . "%");
    if ($company->exists()) {
      $company = $company->get();
      return response()->json($company, 200);
    } else {
      return response()->json(["message" => "No company found with the company name: " . $companyName . "."], 404);
    }
  }

  public function storeCompany(Request $request){
    $company = new Company();
    $company->name = $request->input('name');
    $company->location_id = $request->input('location_id');
    $company->image_location = $request->input('image_location');
    $company->created_at = $request->input('created_at');
    $company->updated_at = $request->input('updated_at');
    
    try {
      $company->save();
      return response()->json($company, 200);
    } catch (\Illuminate\Database\QueryException $e) {
        return response()->json($e, 400);
    }
  }

  public function getCompanies(){
    return Company::all();
  }
}
