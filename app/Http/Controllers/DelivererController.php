<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;
use App\Order;
use App\Product;
use App\Review;

class DelivererController extends Controller
{
  public function getDeliveriesByDelivererId($deliverer_id){
    $deliverer = Deliverer::where('id','=',$deliverer_id)->first();
    $deliveries = $deliverer->getDeliveries;
    foreach($deliveries as $delivery){
      $delivery->getDeliveryLocation = Location::where('id','=',$delivery->deliver_to_location_id)->first();
      $delivery->getOrder;
      $companyId = $delivery->getOrder->ordered_by_company_id;
      $delivery->get_company_info = Company::where('id','=',$companyId)->first();
      $delivery->get_company_location = Location::where('id','=',$delivery->get_company_info->location_id)->first();
    }
    return $deliverer;
  }

  public function getDelivererByDelivererId($deliverer_id) {
    $deliverer = Deliverer::where('id','=',$deliverer_id)->first();
    return $deliverer;
  }
}
