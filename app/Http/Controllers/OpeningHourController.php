<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpeningHour;

class OpeningHourController extends Controller
{
    public function storeOpeningHour(Request $request) {
        $openingHour = new OpeningHour();

        $openingHour->id = $request->input('id');
        $openingHour->monday = $request->input('monday');
        $openingHour->tuesday = $request->input('tuesday');
        $openingHour->wednesday = $request->input('wednesday');
        $openingHour->thursday = $request->input('thursday');
        $openingHour->friday = $request->input('friday');
        $openingHour->saturday = $request->input('saturday');
        $openingHour->sunday = $request->input('sunday');

        try {
            $openingHour->save();
            return response()->json($openingHour, 200);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json($e, 400);
        }
    }
}
