<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;
use App\Order;
use App\Product;
use App\Review;

class CustomerController extends Controller
{
  public function show($id){
  
    return Customer::where('id', '=', $id)->first();
  }
  
  public function storeCustomer(Request $request){
    $customer = new Customer();
    $customer->name = $request->input('name');
    $customer->email = $request->input('email');
    $customer->password = $request->input('password');
    $customer->phoneno = $request->input('phoneno');
    $customer->location_id = $request->input('location_id');
    $customer->order_id = $request->input('order_id');
    $customer->created_at = $request->input('created_at');
    $customer->updated_at = $request->input('updated_at');

    try{
    $customer->save();
    }
    catch(Exception $e){
      return redirect('/');
    }

  }

  public function getCustomers(){
    return Customer::all();
  }
}
