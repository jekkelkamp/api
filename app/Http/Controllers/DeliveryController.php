<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;
use App\Order;
use App\Product;
use App\Review;

class DeliveryController extends Controller
{
  public function getUnassignedDeliveries() {
    $deliveries = Delivery::Where('deliverer_id','=',null)->get();
    foreach($deliveries as $delivery){
      $order = Order::where('id','=',$delivery->order_id)->first();
      $company = Company::where('id','=',$order->ordered_by_company_id)->first();
      $delivery->orderedByCompany = $company;
      $delivery->orderedByCompany->addres = Location::where('id','=',$company->location_id)->first();
      $delivery->deliverLocation = Location::where('id','=',$delivery->deliver_to_location_id)->first();
    }
    return $deliveries;
  }

  public function assignDelivererToDelivery($delivery_id,$deliverer_id) {
    $delivery = Delivery::where('id','=',$delivery_id)->first();
    $delivery->deliverer_id = $deliverer_id;

    try {
      $delivery->save();
    }
    catch(Exception $e) {
      return redirect('/');
    }
  }
}
