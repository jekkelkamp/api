<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;
use App\Order;
use App\Product;
use App\Review;

class ReviewController extends Controller
{
    public function store(Request $request){
      $review = new Review();
      $review->customer_name= $request->input('customer_name');
      $review->company_id = $request->input('company_id');
      $review->review_food = $request->input('review_food');
      $review->review_delivery = $request->input('review_delivery');
      $review->description = $request->input('description');
      $review->created_at = $request->input('created_at');
      $review->updated_at = $request->input('updated_at');

      try {
        $review->save();
      }catch(Exception $e){
        return redirect('/');
      }
    }//store

    public function getReviewByCompanyId($companyId) {
      $reviews = Review::with("customer")->where("company_id", $companyId);
      if ($reviews->exists()) {
        return response()->json($reviews->get(), 200);
      } else {
        return response()->json(["message" => "No reviews for company with company id: " . $companyId . "."], 404);
      }
    }
}
