<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Deliverer;
use App\Delivery;
use App\Location;

use App\Product;
use App\Review;

class LocationController extends Controller
{
  public function storeLocation(Request $request){
    $location = new Location();
    $location->streetname = $request->input('streetname');
    $location->house_number = $request->input('house_number');
    $location->house_number_addition = $request->input('house_number_addition');
    $location->postal_code = $request->input('postal_code');
    $location->created_at = $request->input('created_at');
    $location->updated_at = $request->input('updated_at');

    try {
      $location->save();
      return response()->json($location, 200);
    } catch (\Illuminate\Database\QueryException $e) {
        return response()->json($e, 400);
    }
  }

  public function getLocations(){
    return Location::all();
  }
}
