<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "company";

    function locations() {
      return $this->hasOne('App\Location','id','location_id');
    }

    function openingHour() {
      return $this->hasOne('App\OpeningHour', 'id', 'id');
    }
}
