<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "order";

    function getCompany() {
      return $this->hasOne('App\Company','id','ordered_by_company_id');
    }
}
