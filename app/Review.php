<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "review";

    function customer() {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }
}
