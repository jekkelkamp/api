<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliverer extends Model
{
    protected $table = "deliverer";

    function getDeliveries() {
      return $this->hasMany('App\Delivery','deliverer_id','id');
    }
}
