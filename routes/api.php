<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('product/by-company-id/{companyId}', 'ProductController@getProductsByCompany');
Route::get('product/by-company-id/{companyId}/product-like-name/{productName}', 'ProductController@getProductLikeName');
Route::post('product/storeProduct', 'ProductController@storeProduct');

Route::put('user/updaterole/{role}/{user_id}', 'UserController@updateRole');

Route::get('orders', 'OrderController@getOrders');
Route::get('order/by-order-id/{order_id}','OrderController@getOrdersById');
Route::get('order/by-company-id/{companyId}', 'OrderController@getOrdersByCompanyId');
Route::post('order/storeOrder', 'OrderController@storeOrder');
Route::put('order/update/by-order-id/{order_id}', 'OrderController@update');


Route::get('deliverer/by-deliverer-id/{deliverer_id}','DelivererController@getDelivererByDelivererId');

Route::get('delivery/by-deliverer-id/{deliverer_id}','DelivererController@getDeliveriesByDelivererId');
Route::get('delivery/unassigned','DeliveryController@getUnassignedDeliveries');
Route::put('delivery/assign/{delivery_id}/{deliverer_id}','DeliveryController@assignDelivererToDelivery');

Route::post('company/storeCompany', 'CompanyController@storeCompany');
Route::get('company', 'CompanyController@getAllCompanies');
Route::get('company/by-company-name', 'CompanyController@getCompanyName');
Route::get('company/by-company-id/{companyId}', 'CompanyController@getCompanyById');
Route::get('company/like-company-name/{companyName}', 'CompanyController@getCompanyLikeName');

Route::get('locations', 'LocationController@getLocations');
Route::post('location/storeLocation', 'LocationController@storeLocation');

Route::post('openingshours/storeOpeningshours', 'OpeningHourController@storeOpeningHour');

Route::get('customers', 'CustomerController@getCustomers');
Route::post('customer/storeCustomer', 'CustomerController@storeCustomer');

Route::post('/review/create','ReviewController@store');
Route::get('/review/by-company-id/{companyId}', 'ReviewController@getReviewByCompanyId');

//Login Routes
Route::post('register', 'UserController@register');
Route::get('returnusers', 'UserController@getUsers');
Route::post('registerCompany', 'UserController@register');
Route::post('registerDeliverer', 'UserController@register');
Route::post('login', 'UserController@authenticate');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});
