<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('name');
        $table->string('description');
        $table->longText('photo')->nullable();
        $table->string('allergies');
        $table->float('price');
        $table->foreignId('company_id');
        $table->integer('count')->nullable();
        $table->integer('total')->nullable();
        $table->foreign('company_id')->references('id')->on('company');
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
        $table->timestamp('deleted_at')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function($table){
          $table->dropForeign('product_company_id_foreign');
        });
        Schema::dropIfExists('product');
    }
}
