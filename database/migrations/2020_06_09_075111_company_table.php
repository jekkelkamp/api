<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('company', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('name');
        $table->foreignId('location_id')->nullable();
        $table->longText('image_location')->nullable();
        $table->timeStamp('created_at')->nullable();
        $table->timeStamp('updated_at')->nullable();
        $table->foreign('location_id')->references('id')->on('location');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('company', function($table){
        $table->dropForeign('company_location_id_foreign');
      });
        Schema::dropIfExists('company');
    }
}
