<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('review', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('customer_name');
        $table->foreignId('company_id');
        $table->float('review_food');
        $table->float('review_delivery');
        $table->string('description');
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
        $table->foreign('company_id')->references('id')->on('company');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('review', function($table){
          $table->dropForeign('review_customer_id_foreign');
          $table->dropForeign('review_company_id_foreign');
        });
        Schema::dropIfExists('product');
    }
}
