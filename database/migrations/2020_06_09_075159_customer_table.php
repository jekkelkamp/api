<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('customer', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('name');
        $table->string('email');
        $table->string('password')->nullable();
        $table->string('phoneno');
        $table->foreignId('location_id');
        $table->foreignId('order_id');
        $table->timeStamp('created_at')->nullable();
        $table->timeStamp('updated_at')->nullable();
        $table->foreign('location_id')->references('id')->on('location');
        $table->foreign('order_id')->references('id')->on('order');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer', function($table){
          $table->dropForeign('customer_location_id_foreign');
          $table->dropForeign('customer_order_id_foreign');
        });
        Schema::dropIfExists('customer');
    }
}
