<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DelivererTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('deliverer', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('name');
        $table->string('email')->nullable();
        $table->string('password')->nullable();
        $table->float('lat',9,6)->nullable();
        $table->float('lon',9,6)->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliverer');
    }
}
