<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('delivery', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->foreignId('deliverer_id')->nullable();
        $table->foreignId('order_id');
        $table->foreignId('deliver_to_location_id');
        $table->string('status');
        $table->foreign('deliverer_id')->references('id')->on('deliverer');
        $table->foreign('order_id')->references('id')->on('order');
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery', function($table){
          $table->dropForeign('delivery_deliverer_id_foreign');
          $table->dropForeign('delivery_order_id_foreign');
          $table->dropForeign('delivery_deliver_to_location_id_foreign');
        });
        Schema::dropIfExists('deliverer');
    }
}
