<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('products_in_order')->nullable();
        $table->foreignId('ordered_by_company_id')->nullable();
        $table->float('costs')->nullable();
        $table->string('payment_method')->nullable();
        $table->boolean('payment_complete')->nullable();
        $table->string('delivery_time')->nullable();
        $table->text('description')->nullable();
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
        $table->timestamp('deleted_at')->nullable();
        $table->foreign('ordered_by_company_id')->references('id')->on('company');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function($table){
          $table->dropForeign('order_ordered_by_company_id_foreign');
        });
        Schema::dropIfExists('order');
    }
}
