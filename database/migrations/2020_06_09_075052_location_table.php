<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('location', function (Blueprint $table) {
        $table->id('id')->unique();
        $table->string('streetname');
        $table->integer('house_number');
        $table->string('house_number_addition')->nullable();
        $table->string('postal_code');
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
