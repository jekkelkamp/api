<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('location')->insert([
      'id' =>  1,
      'streetname' => 'Prinses Irenelaan',
      'house_number' => 76,
      'house_number_addition' => '',
      'postal_code' => '2404BJ',
    ]);

      DB::table('location')->insert([
      'id' =>  2,
      'streetname' => 'Provinciepassage',
      'house_number' => 35,
      'house_number_addition' => '',
      'postal_code' => '2408GT',
    ]);

      DB::table('location')->insert([
      'id' =>  3,
      'streetname' => 'Hooftstraat',
      'house_number' => 28,
      'house_number_addition' => '',
      'postal_code' => '2406GL',
    ]);

      DB::table('location')->insert([
      'id' =>  4,
      'streetname' => 'Slenke',
      'house_number' => 4,
      'house_number_addition' => 'a',
      'postal_code' => '7683PA',
    ]);

      DB::table('location')->insert([
      'id' =>  5,
      'streetname' => 'Rohorst',
      'house_number' => 8,
      'house_number_addition' => '',
      'postal_code' => '7683PE',
      ]);

      DB::table('location')->insert([
      'id' =>  6,
      'streetname' => 'Ommerweg',
      'house_number' => 5,
      'house_number_addition' => '',
      'postal_code' => '7683AV',
      ]);

      DB::table('location')->insert([
      'id' =>  7,
      'streetname' => 'Zomerweg',
      'house_number' => 44,
      'house_number_addition' => '',
      'postal_code' => '7683PR',
      ]);

      DB::table('location')->insert([
      'id' =>  8,
      'streetname' => 'Giessenburg',
      'house_number' => 38,
      'house_number_addition' => '',
      'postal_code' => '2135EN',
      ]);

      DB::table('location')->insert([
        'id' =>  9,
        'streetname' => 'Clingenburg',
        'house_number' => 36,
        'house_number_addition' => '',
        'postal_code' => '2135CC',
      ]);

    }
}
