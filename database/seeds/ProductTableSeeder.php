<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('product')->insert([
      'id' =>  1,
      'name' => 'BBQ Bacon & Chicken Pizza',
      'description' => 'Pizza met BBQ-saus, mozzarella, rode ui, paprika, chicken, bacon (100% kip), een swirl van BBQ-saus en knoflookolie op de rand',
      'allergies' => 'Gluten',
      'price' => 8.99,
      'company_id'=> 1,
      'count' => 0,
      'total' => 0,
    ]);

      DB::table('product')->insert([
      'id' =>  2,
      'name' => 'Double Pepperoni',
      'description' => 'Pizza met tomatensaus, 100% mozzarella, pepperoni (Amerikaanse salami), nog meer pepperoni en extra mozzarella. Met boterzachte knoflookolie op de rand van de pizza',
      'allergies' => 'Gluten',
      'price' => 9.99,
      'company_id'=> 1,
      'count' => 0,
      'total' => 0,
    ]);

      DB::table('product')->insert([
      'id' =>  3,
      'name' => 'East Side Shoarma Pizza',
      'description' => 'Pizza met tomatensaus, 100% mozzarella, shoarma (100% kip), extra mozzarella en een knoflookswirl. Met boterzachte knoflookolie op de rand van de pizza',
      'allergies' => 'Gluten',
      'price' => 11.49,
      'company_id'=> 1,
      'count' => 0,
      'total' => 0,
    ]);

      DB::table('product')->insert([
      'id' =>  4,
      'name' => 'Tempura ebi roll',
      'description' => 'Sesamzaad, tempura ebi, mayonaise, komkommer en avocado',
      'allergies' => '',
      'price' => 5.00,
      'company_id'=> 2,
      'count' => 0,
      'total' => 0,
    ]);

      DB::table('product')->insert([
      'id' =>  5,
      'name' => 'Maki mix',
      'description' => '24 stuks: 6 kappa maki, 6 kani maki, 6 tonijn maki, 6 zalm maki',
      'allergies' => '',
      'price' => 10.00,
      'company_id'=> 2,
    ]);

      DB::table('product')->insert([
      'id' =>  6,
      'name' => 'Kani nigiri',
      'description' => 'Surimi krab',
      'allergies' => '',
      'price' => 1.60,
      'company_id'=> 2,
    ]);

      DB::table('product')->insert([
      'id' =>  7,
      'name' => 'Saté',
      'description' => 'Onze beroemde saté! Keuze uit 3, 4 of 5 stokjes',
      'allergies' => 'Kip, Varkenshaas en Franse friet',
      'price' => 14.50,
      'company_id'=> 3,
    ]);

      DB::table('product')->insert([
      'id' =>  8,
      'name' => 'Spareribs',
      'description' => 'Keuze uit: 2 halve ribben, 3 halve ribben, Naturel, Kruidenboter, Honing, Ketjap en meer',
      'allergies' => '',
      'price' => 15.50,
      'company_id'=> 3,
    ]);

      DB::table('product')->insert([
      'id' =>  9,
      'name' => 'Spicy chicken wings',
      'description' => '5 stuks. Keuze uit: Chilisaus, knoflooksaus, BBQ saus, Chunky Ketchup, Truffelmayonaise, Fritessaus en meer',
      'allergies' => '',
      'price' => 4.95,
      'company_id'=> 3,
    ]);

      DB::table('product')->insert([
      'id' =>  10,
      'name' => '650 gram spare ribs',
      'description' => 'Een flinke portie van 650 gram spareribs in een smaak naar keuze. Maak het menu compleet door een smakelijk bijgerecht toe te voegen en maak het af met de overheerlijke, zelfgemaakte knoflooksaus',
      'allergies' => '',
      'price' => 16.50,
      'company_id'=> 4,
    ]);

      DB::table('product')->insert([
      'id' =>  11,
      'name' => 'Jalapeno Burger Los',
      'description' => 'De jalapeno Burger XL is een extra large burger met vlezige jalapenos en een zelf bereide creamy chili mayo saus. Wij serveren hem op een ovenverse zuurdesem flaguette met verse groenten en rode ui.',
      'allergies' => '',
      'price' => 10.95,
      'company_id'=> 4,
    ]);

      DB::table('product')->insert([
      'id' =>  12,
      'name' => 'Zalm',
      'description' => 'Geniet van deze mooie zalmmoot, bereid in een dilledressing van extra vierge olijfolie. Een licht gerecht dat ontzettend lekker is!',
      'allergies' => '',
      'price' => 16.50,
      'company_id'=> 4,
    ]);

      DB::table('product')->insert([
      'id' =>  13,
      'name' => 'Pita gyros',
      'description' => 'Pitabroodje met gyros, tzatziki, ijsbergsla, ui en tomaat',
      'allergies' => '',
      'price' => 5.75,
      'company_id'=> 5,
    ]);

      DB::table('product')->insert([
      'id' =>  14,
      'name' => 'Gyros schotel',
      'description' => 'Gyros, ui en friet',
      'allergies' => '',
      'price' => 13.50,
      'company_id'=> 5,
    ]);

      DB::table('product')->insert([
      'id' =>  15,
      'name' => 'Kapsalon gyros (groot)',
      'description' => 'Gyros, patat, knoflooksaus, kaas en sla',
      'allergies' => '',
      'price' => 9.00,
      'company_id'=> 5,
    ]);

      DB::table('product')->insert([
      'id' =>  16,
      'name' => 'Klassieker',
      'description' => 'Rundvleesburger van 120 gram op een sesambol met rode ui, sla, tomaat en Burger Relish.',
      'allergies' => '',
      'price' => 4.95,
      'company_id'=> 6,
    ]);

      DB::table('product')->insert([
      'id' =>  17,
      'name' => 'Kiplekker',
      'description' => 'Kipburger van 110 gram kipfilet op een sesambol met tomaat, sla en joppiesaus.',
      'allergies' => '',
      'price' => 5.75,
      'company_id'=> 6,
    ]);

      DB::table('product')->insert([
      'id' =>  18,
      'name' => 'Meesterlijk',
      'description' => 'Kipfiletburger van 110 gram op een sesambol met sla, tomaat, kaas, bacon strips en joppiesaus.',
      'allergies' => '',
      'price' => 5.75,
      'company_id'=> 6,
    ]);
    }
}
