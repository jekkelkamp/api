<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('customer')->insert([
      'id' =>  1,
      'name' => 'Lotte van t Hof',
      'email' => 'lotte.hof@doitbetter.nl',
      'password' => 'hallo',
      'phoneno' => '0612893169',
      'location_id' => 8,
      'order_id' => 1,
    ]);
    }
}
