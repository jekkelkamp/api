<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            LocationTableSeeder::class,
            DelivererTableSeeder::class,
            CompanyTableSeeder::class,
            OrderTableSeeder::class,
            ProductTableSeeder::class,
            DeliveryTableSeeder::class,
            CustomerTableSeeder::class,
            ReviewTableSeeder::class,
            RolesTableSeeder::class,
            OpeningHourSeeder::class
        ]);
    }
}
