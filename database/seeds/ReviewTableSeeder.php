<?php

use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 7; $i++) {
            DB::table('review')->insert([
                'customer_name' => 'Piet',
                'company_id' => $i,
                'review_food' => 7.6,
                'review_delivery' => 8.5,
                'description' => "Het was echt fantastisch."
            ]);
        }
    }
}
