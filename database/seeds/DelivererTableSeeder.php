<?php

use Illuminate\Database\Seeder;

class DelivererTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('deliverer')->insert([
      'id' =>  1,
      'name' => 'Jannes',
      'lat' => 52.476579,
      'lon' => 6.494644,
    ]);
    }
}
