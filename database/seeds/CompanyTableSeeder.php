<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('company')->insert([
      'name' => 'New York Pizza',
      'location_id' => 1,
      'image_location' => 'http://ipmedt4.applepi.nl/img/company/NewYorkPizza.png',
      ]);

      DB::table('company')->insert([
        'name' => 'Sushi Alphen',
        'location_id' => 2,
        'image_location' => 'http://ipmedt4.applepi.nl/img/company/sushialphen.png',
      ]);

      DB::table('company')->insert([
        'name' => 'De Beren',
        'location_id' => 3,
        'image_location' => 'http://ipmedt4.applepi.nl/img/company/beren.png'
      ]);

      DB::table('company')->insert([
        'name' => 'Spare Rib Express',
        'location_id' => 6,
        'image_location' => 'http://ipmedt4.applepi.nl/img/company/spareribs.png'
      ]);

      DB::table('company')->insert([
        'name' => 'Olivo',
        'location_id' => 7,
        'image_location' => 'http://ipmedt4.applepi.nl/img/company/olivo.png'
      ]);

      DB::table('company')->insert([
        'name' => 'Foodmaster',
        'location_id' => 6,
        'image_location' => 'http://ipmedt4.applepi.nl/img/company/foodmaster.png'
      ]);
    }
}
