<?php

use Illuminate\Database\Seeder;

class DeliveryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('delivery')->insert([
      'order_id' => 1,
      'deliver_to_location_id' => 4,
      'status' => 'ready for pickup',
    ]);

    DB::table('delivery')->insert([
    'order_id' => 2,
    'deliver_to_location_id' => 5,
    'status' => 'ready for pickup',
    ]);

    DB::table('delivery')->insert([
    'order_id' => 3,
    'deliver_to_location_id' => 8,
    'status' => 'ready for pickup',
    ]);

    DB::table('delivery')->insert([
    'order_id' => 4,
    'deliver_to_location_id' => 9,
    'status' => 'ready for pickup',
    ]);

    }
}
