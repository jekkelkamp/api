<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('order')->insert([
      'id' =>  1,
      'products_in_order' => 'test',
      'ordered_by_company_id' => 4,
      'costs' => 5.50,
      'delivery_time' => '10:10',
      'payment_method' => 'cash',
      'payment_complete' => false,
    ]);

      DB::table('order')->insert([
      'id' =>  2,
      'products_in_order' => 'test',
      'ordered_by_company_id' => 5,
      'costs' => 10.20,
      'delivery_time' => '10:10',
      'payment_method' => 'cash',
      'payment_complete' => false,
    ]);

    DB::table('order')->insert([
    'id' =>  3,
    'products_in_order' => 'test',
    'ordered_by_company_id' => 1,
    'costs' => 10.20,
    'delivery_time' => '10:10',
    'payment_method' => 'cash',
    'payment_complete' => false,
  ]);

    DB::table('order')->insert([
    'id' =>  4,
    'products_in_order' => 'test',
    'ordered_by_company_id' => 2,
    'costs' => 10.20,
    'delivery_time' => '10:10',
    'payment_method' => 'cash',
    'payment_complete' => false,
  ]);

    DB::table('order')->insert([
    'id' =>  5,
    'products_in_order' => 'test',
    'ordered_by_company_id' => 1,
    'costs' => 10.20,
    'delivery_time' => '10:10',
    'payment_method' => 'cash',
    'payment_complete' => false,
  ]);


    }
}
