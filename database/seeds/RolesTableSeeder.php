<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
        'name' =>  'customer',
      ]);

      DB::table('roles')->insert([
        'name' =>  'company',
      ]);

      DB::table('roles')->insert([
        'name' =>  'deliverer',
      ]);
    }
}
