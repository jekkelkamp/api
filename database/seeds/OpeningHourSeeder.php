<?php

use Illuminate\Database\Seeder;

class OpeningHourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("opening_hour")->insert([
            'id' => 1,
            'monday' => "08:00 - 22:00",
            'tuesday' => "08:00 - 22:00",
            'wednesday' => "08:00 - 22:00",
            'thursday' => "08:00 - 22:00",
            'friday' => "08:00 - 22:00",
            'saturday' => "08:00 - 22:00",
            'sunday' => "08:00 - 22:00"
        ]);
        DB::table("opening_hour")->insert([
            'id' => 2,
            'monday' => "07:00 - 20:00",
            'tuesday' => "07:00 - 20:00",
            'wednesday' => "07:00 - 20:00",
            'thursday' => "07:00 - 20:00",
            'friday' => "07:00 - 20:00",
            'saturday' => "11:00 - 22:00",
            'sunday' => "11:00 - 22:00"
        ]);
        DB::table("opening_hour")->insert([
            'id' => 3,
            'monday' => "08:00 - 22:00",
            'tuesday' => "08:00 - 22:00",
            'wednesday' => "08:00 - 22:00",
            'thursday' => "08:00 - 22:00",
            'friday' => "08:00 - 22:00",
            'saturday' => "08:00 - 22:00",
            'sunday' => "08:00 - 22:00"
        ]);
        DB::table("opening_hour")->insert([
            'id' => 4,
            'monday' => "08:00 - 22:00",
            'tuesday' => "08:00 - 22:00",
            'wednesday' => "08:00 - 22:00",
            'thursday' => "08:00 - 22:00",
            'friday' => "08:00 - 22:00",
            'saturday' => "08:00 - 22:00",
            'sunday' => "08:00 - 22:00"
        ]);
        DB::table("opening_hour")->insert([
            'id' => 5,
            'monday' => "08:00 - 22:00",
            'tuesday' => "08:00 - 22:00",
            'wednesday' => "08:00 - 22:00",
            'thursday' => "08:00 - 22:00",
            'friday' => "08:00 - 22:00",
            'saturday' => "08:00 - 22:00",
            'sunday' => "08:00 - 22:00"
        ]);
        DB::table("opening_hour")->insert([
            'id' => 6,
            'monday' => "08:00 - 22:00",
            'tuesday' => "08:00 - 22:00",
            'wednesday' => "08:00 - 22:00",
            'thursday' => "08:00 - 22:00",
            'friday' => "08:00 - 22:00",
            'saturday' => "08:00 - 22:00",
            'sunday' => "08:00 - 22:00"
        ]);
    }
}
