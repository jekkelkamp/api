@extends('layout')
@section('content')
  <div class="cardcontainer">
    @foreach($companies as $company)

    <div class="card">
      <div class="carddescription">{{$company->name}}</div>
      <div class="cardheader"><img src={{$company->image_location}} class="cardheader__img" alt=""/></div>

    </div>

    @endforeach

  </div>
@endsection
